**aftermirror**
Official https://aftermirror.com/ website repo.

This repository contains the code used to make the website run as-is.
Features get implemented left and right so expect for new things to come in the future.
But seriously, how did you find out about this?

**credits**

*design*
[HTML5 UP](http://html5up.net/) for the base website template
> [Font Awesome](http://fortawesome.github.com/Font-Awesome) for awesome icons
> html5shiv.js (@afarkas @jdalton @jon_neal @rem)
> [CSS3 Pie](css3pie.com)
> [jquery.scrolly](n33.co)
> [skel](n33.co)

*javascript*
[jQuery](jquery.com)
> [jQuery contextMenu plugin](http://medialize.github.io/jQuery-contextMenu/)
> [jQuery finger](https://github.com/ngryman/jquery.finger)
> [jQuery UI](http://jqueryui.com/)
> [tooltipsy](http://tooltipsy.com/)

*s(tream)watch -- multi client video synchronization thingy*
[node.js](https://nodejs.org/)
> [socket.io](http://socket.io/)

and any others I may have missed.