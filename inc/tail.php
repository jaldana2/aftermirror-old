<!-- Contact -->
<div class="wrapper style4">
	<article id="contact" class="container 75%">
		<header>
			<h2>Contact Me</h2>
			<p>For all your questions, comments and concerns.</p>
		</header>
		<div>
			<span class='icon fa-comment button small' onclick="$(this).fadeOut(200); $('#contactform').fadeIn(400);">&nbsp;&nbsp;&nbsp;show contact form</span>
			<div class="row" id="contactform" style="display: none;">
				<div class="12u">
					<form method="post" action="#">
						<div>
							<div class="row">
								<div class="6u">
									<input type="text" name="name" id="name" placeholder="Name" />
								</div>
								<div class="6u">
									<input type="text" name="email" id="email" placeholder="Email" />
								</div>
							</div>
							<div class="row">
								<div class="12u">
									<input type="text" name="subject" id="subject" placeholder="Subject" />
								</div>
							</div>
							<div class="row">
								<div class="12u">
									<textarea name="message" id="message" placeholder="Message"></textarea>
								</div>
							</div>
							<div class="row 200%">
								<div class="12u">
									<ul class="actions">
										<li><input type="submit" value="Send Message" /></li>
										<li><input type="reset" value="Clear Form" class="alt" /></li>
									</ul>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<footer>
			<ul id="copyright">
				<li>after|<b>mirror</b></li>
				<li><a href="app.Credits">credits</a></li>
				<li>git: <a href="https://bitbucket.org/jaldana2/aftermirror" target="_blank">bitbucket</a></li>
				<li>design: <a href="http://html5up.net">HTML5 UP</a></li>
			</ul>
		</footer>
	</article>
</div>

</body>
</html>
