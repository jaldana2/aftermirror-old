<?php
$app = "Home"; // default app
$uiEnabled = true;

if (isset($_GET["app"])) {
	if (file_exists("app/{$_GET['app']}.php")) {
		$app = $_GET["app"];
	}
	else {
		logError("ERROR_NOAPP", $_ERRORS);
	}
}
if (isset($_GET["core"])) {
	if (file_exists("app/{$_GET['core']}.php")) {
		$app = $_GET["core"];
		$uiEnabled = false;
	}
	else {
		logError("ERROR_NOCORE", $_ERRORS);
	}
}
if (file_exists("app/preload/{$app}.php")) {
	include("app/preload/{$app}.php");
}

if ($uiEnabled) include_once("inc/head.php");
if ($app == "Home") include_once("inc/home.php");
if (file_exists("app/autoload/{$app}.php")) {
	include("app/autoload/{$app}.php");
}
include("app/{$app}.php");
if ($uiEnabled) include_once("inc/tail.php");
?>
