<!-- Home -->
<div class="wrapper style1 first">
	<article class="container" id="top">
		<div class="row">
			<div class="4u">
				<span class="image fit"><img src="images/logo.jpg" alt="" /></span>
			</div>
			<div class="8u">
				<?php showErrors($_ERRORS); ?>
				<header>
					<?php
					if (LOGGED_IN) {
						printf("<h1>hey, %s.</h1>", LOGIN_NAME);
					}
					else {
						echo "<h1>welcome to after|<b>mirror</b>.</h1>";
					}
					?>
				</header>
				<?php
				if (LOGGED_IN) {
					echo "
						<p>Welcome back to the super secret community of awesomeness.</p>
						<a href='/app.News' class='button big scrolly'>What's new?</a>
					";
				}
				else {
					echo "
						<p>I don't know what you're expecting to see here. How did you even get here in the first place? I know... someone must have told you. Now the secret has been revealed. Prepare yourself.</p>
						<a href='/app.Login' class='button big scrolly'>Get Started</a>
					";
				}
				?>
			</div>
		</div>
	</article>
</div>
