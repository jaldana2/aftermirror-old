<!DOCTYPE HTML>
<!--
	Miniport by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
<head>
	<title>after|mirror</title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
	<script src="js/jquery.min.js"></script>
	<script src="js/jquery.scrolly.min.js"></script>
	<script src="js/jquery.finger.min.js"></script>
	<script src="js/jquery.ui.position.js"></script>
	<script src="js/jquery.contextMenu.js"></script>
	<script src="js/socket.io-1.2.0.js"></script>
	<script src="js/skel.min.js"></script>
	<script src="js/init.js"></script>
	<script src="js/tooltipsy.min.js"></script>
	<script src="js/stream.js"></script>
	<noscript>
		<link rel="stylesheet" href="css/skel.css" />
		<link rel="stylesheet" href="css/style.css" />
		<link rel="stylesheet" href="css/style-desktop.css" />
	</noscript>
	<!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
	<!--[if lte IE 9]><link rel="stylesheet" href="css/ie/v9.css" /><![endif]-->
	<link rel="stylesheet" href="css/aftermirror.css" />
	<link rel="stylesheet" href="css/tipped.css" />
	<link rel="stylesheet" href="css/jquery.contextMenu.css" />
	<link rel="stylesheet" href="css/jquery-ui.min.css" />
	<link rel="icon" href="/favicon.ico" type="image/x-icon">
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<script>
		$(function() {
			$('.tooltip').tooltipsy({ offset: [0, 1] });
		});
	</script>
</head>
<body>

<!-- Nav -->
	<nav id="nav">
		<ul class="container">
			<li><a href="/">after|<span style='color: #D3F0EC'>mirror</span></a></li>
			<li><a href="/app.Anime">anime</a></li>
			<li><a href="/app.Music">music</a></li>
			<li><a href="/app.Wallpapers">wallpapers</a></li>
			<?php
			if (LOGGED_IN) {
				printf("
					<li style='background-image: url(core.Data?c=profile-picture&v=" . LOGIN_USERNAME . "); background-position: left center; background-size: 32px; background-repeat: no-repeat; padding-left: 32px;'><a href='/app.Profile'>%s</a></li>
					<li><a href='/app.Login?mode=logout' class='tooltip' title='Logout'><span class='fa fa-power-off' style='color: red;'></span></a></li>
					", LOGIN_NAME);
			}
			else {
				echo "<li><a href='/app.Login?mode=login' class='tooltip' title='Login'><span class='fa fa-power-off' style='color: limegreen;'></span></a></li>";
			}
			?>
		</ul>
	</nav>
