<?php
echo "
<br/>
<div class='wrapper style2'>
	<article id='work'>
";
	if (isset($_GET["err"])) switch ($_GET["err"]) {
		case "invalid":
			printMsg("LOGIN_ERR", "Invalid login details.", "Login", "error");
		break;
	}
echo "
		<header>
			<h1>" . LOGIN_NAME . " <a href='app.Profile-Edit?change=name' class='tooltip' title='Change Name'><span class='icon fa-pencil' style='font-size: 16px;'></span></a></h1>
			<p>( " . LOGIN_USERNAME . " )</p>
		</header>
		<p>
			<div class='container'>
				<div style='width: 128px; height: 128px; display: inline-block; background-image: url(core.Data?c=profile-picture&v=" . LOGIN_USERNAME . "); background-size: cover; background-position: center center; box-shadow: 0px 0px 6px gray;'></div>
				<br/>
				<a href='app.Profile-Edit?change=picture' class='button small'>Change Picture</a>
				<br/>
				<a href='app.Profile-Edit?clear-picture' class='button small'>Clear Picture</a>
				<br/>
				<hr/>
				<a href='app.Profile-Edit?change=password' class='button'>Change Password</a>
			</div>
		</p>
	</article>
</div>
";
?>
