<?php
$change = $_GET["change"];
if (isset($change)) switch($change) {
	case "picture":
		echo "
		<br/>
		<div class='wrapper style2'>
			<article id='work'>
				<header>
					<h1>Change Picture</h1>
				</header>
				<p>
					<div class='container'>
						<div style='width: 128px; height: 128px; display: inline-block; background-image: url(core.Data?c=profile-picture&v=" . LOGIN_USERNAME . "); background-size: cover; background-position: center center; box-shadow: 0px 0px 6px gray;'></div>
						<br/>
						<br/>
						<form action='?upload-image' method='post' enctype='multipart/form-data'>
							<input type='file' name='file' id='file' style='display: none;' onchange=\"$('#submit').click();\" />
							<a href='#' class='button small' onclick=\"$('#file').click(); $('#submit').show();\"><span class='icon fa-folder-open'></span>&nbsp;&nbsp;&nbsp;image from computer</a>
							<br/>
							<input type='text' name='url' id='url' style='display: none;' />
							<a href='#' class='button small' onclick=\"$('#url').val(prompt('URL?')); $('#submit').show(); if ($('#url').val().length > 0) $('#submit').click();\"><span class='icon fa-cloud'></span>&nbsp;&nbsp;&nbsp;image from URL</a>
							<br/>
							<br/>
							<input type='submit' class='button' value='Submit' id='submit' style='display: none;' />
						</form>
						<br/>
						<br/>
					</div>
				</p>
			</article>
		</div>
		";
	break;
	case "name":
		echo "
		<br/>
		<div class='wrapper style2'>
			<article id='work'>
				<header>
					<h1>Change Name</h1>
				</header>
				<p>
					<div class='container'>
						<form action='?change-name' method='post'>
							<input type='text' name='name' placeholder='New Name' value='" . LOGIN_NAME . "' class='tooltip' title='Play nice, please.' maxlength='50' />
							<br/>
							<input type='submit' class='butotn' value='Submit' id='submit' />
						</form>
					</div>
				</p>
			</article>
		</div>
		";
	break;
	case "password":
		echo "
		<br/>
		<div class='wrapper style2'>
			<article id='work'>
				<header>
					<h1>Change Password</h1>
				</header>
				<p>
					<div class='container'>
						<form action='?change-password' method='post'>
							<input type='password' name='password' placeholder='New Password' value='' class='tooltip' title='No confirmations. Remember your login please.' />
							<br/>
							<input type='submit' class='butotn' value='Submit' id='submit' />
						</form>
					</div>
				</p>
			</article>
		</div>
		";
	break;
	default:
		echo "
		<br/>
		<div class='wrapper style2'>
			<article id='work'>
				<header>
					<h1>:(</h1>
				</header>
				<p>
					This feature isn't implemented yet... <b>sorry.</b>
				</p>
			</article>
		</div>
		";
	break;
}
?>
