<?php
echo "
<br/>
<div class='wrapper style2'>
	<article id='work'>
		<header>
			<h1><span class='fa fa-cloud-upload'></span></h1>
		</header>
		<p>
			<div class='container'>
				<div style='width: 128px; height: 128px; display: inline-block; background-image: url(core.Data?c=profile-picture&v=" . LOGIN_USERNAME . "); background-size: cover; background-position: center center; box-shadow: 0px 0px 6px gray;'></div>
				<br/>
				<br/>
				<form action='?upload-music' method='post' enctype='multipart/form-data'>
					<b>MP3 File</b>
					<br/>
					<input type='file' name='file' id='file' />
					<br/>
					<b>Title</b>
					<input type='text' name='title' id='title' placeholder='(leave blank for auto-detect)' />
					<br/>
					<b>Artist</b>
					<input type='text' name='artist' id='artist' placeholder='(leave blank for auto-detect)' />
					<br/>
					<b>Album</b>
					<input type='text' name='album' id='album' placeholder='(leave blank for auto-detect)' />
					<br/>
					<b>Type</b>
					<input type='text' name='type' id='type' placeholder='(leave blank for auto-detect)' />
					<br/>
					<input type='submit' class='button' value='Submit' id='submit' />
				</form>
				<br/>
				<br/>
			</div>
		</p>
	</article>
</div>
";
?>
