<?php
$hostHD = "https://nyc-01.aftermirror.com/anime/high/";
$hostSD = "https://nyc-01.aftermirror.com/anime/low/";

$uniqid = LOGIN_USERNAME . uniqid();
$controls = isset($_GET["controls"]) ? "controls" : "";
echo "
<div class='wrapper style2'>
	<article id='work'>
		<div class='container' style='max-width: 800px; text-align: left;'>
			<p>
				<span><b>{$_GET['anime']}</b> Episode {$_GET['episode']}</span>
				<video {$controls} id='ctlVideo' style='width: 100%; box-shadow: 0px 0px 4px black;'>
					<source src='{$hostSD}{$_GET['src']}' type='video/mp4' />
				</video>
				<div style='position: relative; margin-top: -40px; margin-bottom: -30px; padding-top: 10;'>
					<input class='slidey' type='range' style='width: 100%;' id='ctlTime' min='0' />
					<span id='ctlTimeText' style='position: absolute; right: 0px; top: 20px; font-size: 12px; color: black;'>0:00 / 0:00</span>
				</div>
				<br/>
				<span class='fa fa-play' id='ctlPlayPause'></span> | 
				<span class='fa fa-volume-up' id='ctlVolume'></span> | 
				<span class='fa fa-video-camera' id='ctlQuality'> SD</span>
				<br/>
				<a href='app.Swatch?host={$uniqid}&src={$_GET['src']}&anime={$_GET['anime']}&episode={$_GET['episode']}' style='text-decoration: none;'><span class='fa fa-link'></span> watch together</a>
			</p>
		</div>
	</article>
</div>
<script>
	var hdroot = \"{$hostHD}{$_GET['src']}\";
	var sdroot = \"{$hostSD}{$_GET['src']}\";
	$(function() {
		video = document.getElementById('ctlVideo');
	});
</script>
";
?>
