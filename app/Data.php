<?php
if (isset($_GET["c"])) switch($_GET["c"]) {
	case "profile-picture":
		if (file_exists("blob/profile/{$_GET['v']}")) {
			header("Content-Type: image/jpg");
			readfile("blob/profile/{$_GET['v']}");
		}
		else {
			header("Content-Type: image/jpg");
			readfile("images/logo.jpg");
		}
	break;
	case "music-download":
		if (file_exists("blob/music/{$_GET['v']}.mp3")) {
			$db = new SQLite3("db/music.db3");
		
			$query = sprintf("SELECT * FROM music WHERE udid = \"%s\"", $_GET["v"]);
			$result = $db->query($query);
			
			if ($result && $row = $result->fetchArray()) {
				$file = "blob/music/{$_GET['v']}.mp3";
				header("Content-Type: audio/mp3");
				header("Content-length: " . filesize($file));
				header('Content-Type: application/octet-stream');
				header(sprintf("Content-Disposition: attachment; filename=%s - %s.mp3", $row["artist"], $row["title"]));
				readfile($file);
			}
			else {
				echo "<h1>500 - Internal Server Error</h1>";
			}
		}
		else {
			header("HTTP/1.0 404 Not Found", true, 404);
			echo "
				<?xml version='1.0' encoding='iso-8859-1'?>
				<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN'
						 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
				<html xmlns='http://www.w3.org/1999/xhtml' xml:lang='en' lang='en'>
				 <head>
				  <title>404 - Not Found</title>
				 </head>
				 <body>
				  <h1>404 - Not Found</h1>
				 </body>
				</html>
			";
			die();
		}
	break;
}
?>
