<?php
$mode = "dialog";
if (isset($_GET["mode"])) $mode = $_GET["mode"];
if (isset($mode) && $mode == "proc_login") {
	$db = new SQLite3("db/login.db3");
	
	$query = sprintf("SELECT * FROM login WHERE username = \"%s\" AND password = \"%s\"", $db->escapeString(strtolower($_POST["username"])), sha1(AM_HASHSALT . $_POST["password"]));
	$result = $db->query($query);
	
	if ($result = $result->fetchArray()) {
		$udid = sha1($_POST["username"] . uniqid());
		printMsg("", "Welcome back, {$_POST['username']}.", "Login success", "success");
		$query = sprintf("INSERT INTO session (udid, userid, ipaddr, useragent) VALUES (\"%s\", %d, \"%s\", \"%s\")",
			$udid, // udid
			$result["userid"],
			getenv('HTTP_CLIENT_IP') ? : getenv('HTTP_X_FORWARDED_FOR') ? : getenv('HTTP_X_FORWARDED') ? : getenv('HTTP_FORWARDED_FOR') ? : getenv('HTTP_FORWARDED') ? : getenv('REMOTE_ADDR'),
			$_SERVER['HTTP_USER_AGENT']
		);
		// htmlentities works for content.
		if ($db->exec($query)) {
			if (isset($_POST["remember"]) && $_POST["remember"] == "yes") {
				setcookie("am_userid", $result["userid"], time() + (60 * 60 * 24 * 365));
				setcookie("am_sessionkey", $udid, time() + (60 * 60 * 24 * 365));
			}
			else {
				setcookie("am_userid", $result["userid"]);
				setcookie("am_sessionkey", $udid);
			}
			header("Location: /?welcome");
		}
		else {
			header("Location: ?mode=login&err=dberror");
		}
	}
	else {
		setcookie("am_userid", "", time() - 3600);
		setcookie("am_sessionkey", "", time() - 3600);
		header("Location: ?mode=login&err=invalid");
	}
}
elseif (isset($mode) && $mode == "logout") {
	setcookie("am_userid", "", time() - 3600);
	setcookie("am_sessionkey", "", time() - 3600);
	header("Location: /?goodbye");
}

if(LOGGED_IN) { header("Location: /?welcome"); die(); }
?>
