<?php
if(!LOGGED_IN) { header("Location: /app.Login?mode=login&err=notloggedin"); die(); }

if (isset($_GET["upload-music"])) {
	$udid = sha1(uniqid());
	$target = "blob/music/{$udid}.mp3";
	if (isset($_FILES["file"]["name"]) && isSomething($_FILES["file"]["name"])) {
		$fext = fext($_FILES["file"]["name"]);
		if (!fextIsMusic($_FILES["file"]["name"])) {
			header("Location: ?err=invalid-file-ext");
			die();
		}
		if (move_uploaded_file($_FILES["file"]["tmp_name"], $target)) {
			$tags = tagReader($target);
			
			$tagd = array(
				"title" => $tags["Title"],
				"artist" => $tags["Author"],
				"album" => $tags["Album"],
				"type" => ""
			);
			
			if (isSomething($_POST["title"])) $tagd["title"] = $_POST["title"];
			if (isSomething($_POST["artist"])) $tagd["artist"] = $_POST["artist"];
			if (isSomething($_POST["album"])) $tagd["album"] = $_POST["album"];
			if (isSomething($_POST["type"])) $tagd["type"] = $_POST["type"];
			
			$mp3file = new MP3File($target);
			$dur = $mp3file->getDuration();
			
			$db = new SQLite3("db/music.db3");
			// Create DB stuff.
			$query = "
				CREATE TABLE IF NOT EXISTS music (
					udid STRING PRIMARY KEY,
					title STRING,
					artist STRING,
					album STRING,
					type STRING,
					duration INTEGER,
					uploader STRING,
					dateuploaded INTEGER,
					playcount INTEGER,
					rating REAL,
					ratingcount INTEGER
				);
				"; 
			$db->exec($query); // Create table if not exist
			$query = sprintf("INSERT INTO music (udid, title, artist, album, type, uploader, dateuploaded, playcount, rating, ratingcount, duration) VALUES (\"%s\", \"%s\", \"%s\", \"%s\", \"%s\", \"%s\", %d, %d, %f, %d, %d)",
				$udid, // udid
				$db->escapeString($tagd["title"]),
				$db->escapeString($tagd["artist"]),
				$db->escapeString($tagd["album"]),
				$db->escapeString($tagd["type"]),
				LOGIN_USERNAME,
				time(),
				0,
				0,
				0,
				$dur
			);
			// htmlentities works for content.
			if ($db->exec($query)) {
				header("Location: /app.Music-Upload?upload-success");
			}
			else {
				header("Location: ?err=invalid-file-db");
			}
		} else {
			header("Location: ?err=invalid-file-upl");
		}
	}
}
?>
