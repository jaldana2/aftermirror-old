<?php
if(!LOGGED_IN) { header("Location: /app.Login?mode=login&err=notloggedin"); die(); }

if (isset($_GET["upload-image"])) {
	$target = "blob/profile/" . LOGIN_USERNAME;
	if (isset($_FILES["file"]["name"]) && isSomething($_FILES["file"]["name"])) {
		$fext = fext($_FILES["file"]["name"]);
		if (!getimagesize($_FILES["file"]["tmp_name"]) || !fextIsImage($_FILES["file"]["name"])) {
			header("Location: ?change=picture&err=invalid-profile-picture");
			die();
		}
		if (file_exists($target)) unlink($target);
		if (file_exists($target . "-min")) unlink($target . "-min");
		if (move_uploaded_file($_FILES["file"]["tmp_name"], $target)) {
			iTF($target, $target . "-min", 240, 94);
			header("Location: /app.Profile?image-upload-success");
		} else {
			header("Location: ?change=picture&err=invalid-profile-picture");
		}
	}
	elseif (isset($_POST["url"]) && isSomething($_POST["url"])) {
		$content = file_get_contents($_POST["url"]);
		if (isSomething($content)) {
			file_put_contents($target, $content);
			iTF($target, $target . "-min", 240, 94);
			header("Location: /app.Profile?image-upload-success");
		}
		else {
			header("Location: ?change=picture&err=invalid-profile-picture");
		}
	}
}
elseif (isset($_GET["clear-picture"])) {
	$target = "blob/profile/" . LOGIN_USERNAME;
	if (file_exists($target)) unlink($target);
	if (file_exists($target . "-min")) unlink($target . "-min");
	header("Location: /app.Profile?image-clear-success");
}
elseif (isset($_GET["change-name"])) {
	if (isset($_POST["name"]) && isSomething($_POST["name"])) {
		$db = new SQLite3("db/login.db3");
	
		$query = sprintf("UPDATE login SET name = \"%s\" WHERE username = \"%s\"", $db->escapeString($_POST["name"]), LOGIN_USERNAME);
		if ($db->exec($query)) {
			header("Location: /app.Profile?name-success");
		}
		else {
			header("Location: /app.Profile?name-failed");
		}
	}
}
elseif (isset($_GET["change-password"])) {
	if (isset($_POST["password"]) && isSomething($_POST["password"])) {
		$db = new SQLite3("db/login.db3");
	
		$query = sprintf("UPDATE login SET password = \"%s\" WHERE username = \"%s\"", sha1(AM_HASHSALT . $_POST["password"]), LOGIN_USERNAME);
		if ($db->exec($query)) {
			header("Location: /app.Profile?pw-success");
		}
		else {
			header("Location: /app.Profile?pw-failed");
		}
	}
}
?>
