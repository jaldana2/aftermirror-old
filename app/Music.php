<?php
echo "
<br/>
<br/>
<div class='toolbar' style='position: fixed; width: 100%; background-color: rgba(255, 255, 255, 0.75); padding: 0px 10px;'>
	<span class='fa fa-backward tooltip' id='audio_rev' title='Previous'></span>
	&nbsp;
	<span class='fa fa-play tooltip' id='audio_play' title='Play'></span>
	&nbsp;
	<span class='fa fa-forward tooltip' id='audio_next' title='Next'></span>
	&nbsp;
	&nbsp;
	&nbsp;
	<span class='fa fa-volume-down tooltip' id='audio_vol_down' title='Volume Down'></span>
	&nbsp;
	<input type='range' min='0' max='1' value='1' step='0.1' id='audio_vol' style='position: relative; top: 3px;'>
	&nbsp;
	<span class='fa fa-volume-up tooltip' id='audio_vol_up' title='Volume Up'></span>
	&nbsp;
	&nbsp;
	&nbsp;
	<span class='fa fa-repeat tooltip' id='audio_autoplay' title='Auto Play' style='color: #87C3E8;'></span>
	&nbsp;
	<span class='fa fa-headphones tooltip' id='audio_shuffle' title='Shuffle'></span>
	&nbsp;
	&nbsp;
	&nbsp;
	<span id='audio_ticker'>Nothing Currently Playing...</span>
	<span id='audio_dur'></span>
</div>
<div style='margin-top: 32px; padding: 10px;'>
	<table class='music'>
		<tr id='media_header' style='color: black;'>
			<th style='width: 2%;'>&nbsp;</th>
			<th class='title' style='width: 25%;'>Title</th>
			<th style='width: 5%;'>Time</th>
			<th class='artist' style='width: 20%;'>Artist</th>
			<th style='width: 20%;'>Album</th>
			<th style='width: 9%;'>Type</th>
			<th style='width: 8%;'>Rating</th>
			<th style='width: 8%;'>Plays</th>
		</tr>
		";
		$db = new SQLite3("db/music.db3");
		
		$query = "SELECT * FROM music";
		$result = $db->query($query);
		
		$filtervar = "!@#$%^&*();:[]{}<>/|\\'\"_-+";
		if ($result) while ($row = $result->fetchArray()) {
			printf("
				<tr class='media' media-id='%s' id='r%s'>
					<td id='icon_%s'></th>
					<td class='title'>%s</th>
					<td class='duration'>%s</th>
					<td class='artist'>%s</th>
					<td class='album'>%s</th>
					<td class='type'>%s</th>
					<td>%0.2f</th>
					<td>%d</th>
				</tr>
			",
			$row["udid"],
			$row["udid"],
			$row["udid"],
			ltrim($row["title"], $filtervar),
			ltrim(gmdate("i:s", $row["duration"]), "0:"),
			ltrim($row["artist"], $filtervar),
			ltrim($row["album"], $filtervar),
			$row["type"],
			$row["rating"],
			$row["playcount"]
			);
		}
		echo "
	</table>
	<p style='text-align: center;'>
		<br/>
		<br/>
		<a href='app.Music-Upload' class='button small'><span class='icon fa-cloud-upload'></span>&nbsp;&nbsp;Upload a new track</a>
	</p>
</div>
<audio id='audio' style='display: none;'></audio>
<script>
	var audio = document.getElementById('audio');
	var activeudid = '';
	var shuffle = false;
	var autoplay = true;
	var durationtxt = '00:00';
	
	function str_pad_left(string,pad,length){ return (new Array(length+1).join(pad)+string).slice(-length); }
	
	$(function() {
		$('tr.media').on('dblclick', function() {
			playID($(this).attr('media-id'));
		}).on('doubletap', function() {
			playID($(this).attr('media-id'));
		});
		$('#audio_play').on('click', function() {
			if (audio.paused) audio.play();
			else audio.pause();
		});
		$('#audio_vol').on('change', function() {
			audio.volume = $(this).val();
		});
		$('#audio_vol_down').on('click', function() {
			$('#audio_vol').val(0.0);
			audio.volume = 0.0;
		});
		$('#audio_vol_up').on('click', function() {
			$('#audio_vol').val(1.0);
			audio.volume = 1.0;
		});
		audio.onplay = function() {
			$('#audio_play').removeClass('fa-play');
			$('#audio_play').addClass('fa-pause');
		}
		audio.onpause = function() {
			$('#audio_play').removeClass('fa-pause');
			$('#audio_play').addClass('fa-play');
		}
		audio.onended = function() {
			$('#audio_play').removeClass('fa-pause');
			$('#audio_play').addClass('fa-play');
			playNext();
		}
		$('#audio_autoplay').on('click', function() {
			if (autoplay) {
				$(this).css({ color: 'inherit' });
				autoplay = false;
			}
			else {
				$(this).css({ color: '#87C3E8' });
				autoplay = true;
			}
		});
		$('#audio_shuffle').on('click', function() {
			if (shuffle) {
				$(this).css({ color: 'inherit' });
				shuffle = false;
			}
			else {
				$(this).css({ color: '#87C3E8' });
				shuffle = true;
			}
		});
		$('#audio_next').on('click', function() {
			playNext();
		});
		$.contextMenu({
			selector: 'tr.media',
			items: {
				download: {
					name: 'Download',
					callback: function(key, opt) { ctDownloadDialog($(this).attr('media-id')); }
				},
				edit: {
					name: 'Edit...',
					callback: function(key, opt) { ctEditDialog($(this).attr('media-id')); }
				}
			}
		});
	});
	function ctDownloadDialog(key) {
		//alert('Download: ' + key);
		document.location = 'core.Data?c=music-download&v=' + key;
	}
	function ctEditDialog(key) {
		alert('Edit: ' + key);
	}
	
	function playID(id) {
		$('tr.active').removeClass('active');
		$('#r' + id).addClass('active');
		activeudid = id;
		audio.src = 'blob/music/' + id + '.mp3';
		audio.play();
		$('#audio_ticker').text(
			$('#r' + id).find('.artist').html()
			+ ' - ' +
			$('#r' + id).find('.title').html()
		);
		durationtxt = $('#r' + id).find('.duration').html();
		$('html, body').animate({
			scrollTop: $('#r' + id).offset().top - 125
		}, 500);
	}
	function playNext() {
		var next = $('#r' + activeudid).next('tr').attr('media-id');
		if (!next) {
			next = $('#media_header').next('tr').attr('media-id');
		}
		if (autoplay) {
			playID(next);
		}
	}
	function updTime() {
		var time = Math.floor(audio.currentTime);
		var minutes = Math.floor(time / 60);
		var seconds = time - (minutes * 60);
		var ctime = str_pad_left(minutes,'0',2) + ':' + str_pad_left(seconds,'0',2);
		$('#audio_dur').text(
			ctime
			+ ' / ' + 
			durationtxt
		);
	}
	setInterval(\"updTime()\", 1000);
</script>
";
?>
