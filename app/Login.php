<?php
$mode = "dialog";
if (isset($_GET["mode"])) $mode = $_GET["mode"];
if (isset($mode) && $mode == "login") {
	echo "
	<br/>
	<div class='wrapper style2'>
		<article id='work'>
	";
	if (isset($_GET["err"])) {
		switch ($_GET["err"]) {
			case "invalid":
				printMsg("LOGIN_ERR", "Invalid login details.", "Login", "error");
			break;
			case "notloggedin":
				printMsg("LOGIN_ERR", "You must be logged in to view this content.", "Login", "error");
			break;
			default:
				printMsg("LOGIN_ERR", "Generic error", "Login", "error");
			break;
		}
		echo "<br/><br/>";
	}
	echo "
			<header>
				<h2>Login</h2>
				<p>You need an account to do... stuff.</p>
			</header>
			<p>
			<div class='container formify'>
				<form action='?mode=proc_login' method='post'>
					<input type='text' name='username' placeholder='Username' />
					<input type='password' name='password' placeholder='Password' />
					<input type='checkbox' name='remember' value='yes' id='remember' /> <label for='remember' style='display: inline;'>Remember me?</label>
					<br/>
					<br/>
					<input type='submit' value='Log In' />
				</form>
				<br/>
				<br/>
				<a href='?mode=gplus-login' class='button scrolly'>Log in using Google+</a>
				<br/>
				<br/>
				If you do not have an account...
				<br/>
				<a href='?mode=register' class='button scrolly'>Sign Up</a>
			</div>
			</p>
		</article>
	</div>
	";
}
elseif (isset($mode) && $mode == "register") {
	echo "
	<br/>
	<div class='wrapper style2'>
		<article id='work'>
			<header>
				<h2>Login</h2>
				<p>You need an account to do... stuff.</p>
			</header>
			<div class='container' style='width: 80%;'>
				<form action='?mode=proc_register' method='post'>
					<h4>The usual stuff...</h4>
					<input type='text' name='username' placeholder='* Username (you need this to login)' value='' />
					<input type='password' name='password' placeholder='* Password (make sure you type it right)' value='' />
					<h4>Make up a new identity...</h4>
					<input type='text' name='name' placeholder='* Name (nickname, username, whatever you want)' value='' />
					<h4>Just in case...</h4>
					<input type='text' name='email' 
					placeholder='Email (optional, for account recovery)' value='' />
					<input type='text' name='secquestion' 
					placeholder='Security Question (optional)' value='' />
					<input type='text' name='secanswer' 
					placeholder='Security Answer (optional, case sensitive)' value='' />
					
					<input type='submit' value='Sign Me Up!' />
				</form>
			</div>
			<p>
				<br/>
				<br/>
				If you're already registered...
				<br/>
				<a href='?mode=login' class='button scrolly'>Login</a>
			</p>
		</article>
	</div>
	";

}
elseif (isset($mode) && $mode == "gplus-login") {
	echo "
	<br/>
	<div class='wrapper style2'>
		<article id='work'>
			<header>
				<h2>Login</h2>
				<p>You need an account to do... stuff.</p>
			</header>
			<p>
				Google+ integration isn't up just yet. Come back later.
				<br/>
				<br/>
				If you're already registered...
				<br/>
				<a href='?mode=login' class='button scrolly'>Login normally</a>
			</p>
		</article>
	</div>
	";

}
elseif (isset($mode) && $mode == "proc_register") {
	echo "
	<br/>
	<div class='wrapper style2'>
		<article id='work'>
			<header>
				<h2>Login</h2>
				<p>You need an account to do... stuff.</p>
			</header>
			<p>
	";
	$verificationPassed = true;
	if (!isset($_POST["username"]) || cleanANString($_POST["username"]) == "") {
		$verificationPassed = false;
		logError("Username cannot be blank.", $_ERROR);
	}
	if (!isset($_POST["password"]) || $_POST["password"] == "") {
		$verificationPassed = false;
		logError("Password cannot be blank.", $_ERROR);
	}
	if (!isset($_POST["name"]) || $_POST["name"] == "") {
		$verificationPassed = false;
		logError("Name cannot be blank.", $_ERROR);
	}
	if (isset($_POST["sec-answer"]) && !isset($_POST["sec-question"])) {
		$verificationPassed = false;
		logError("You cannot set a security answer without a valid security question", $_ERROR);
	}
	if (isset($_POST["email"]) && isSomething($_POST["email"]) && !isValidEmail($_POST["email"])) {
		$verificationPassed = false;
		logError("Invalid email address.", $_ERROR);
	}
	
	if (showErrors($_ERROR)) {
		echo "<br/><br/><a href='javascript: history.go(-1);'><b>Go back</b></a>";
	}
	else {
		try {
			$db = new SQLite3("db/login.db3");
			
			// Create DB stuff.
			$query = "
				CREATE TABLE IF NOT EXISTS login (
					userid INTEGER PRIMARY KEY,
					username STRING,
					password STRING,
					name STRING,
					email STRING,
					secquestion STRING,
					secanswer STRING
				);
				CREATE TABLE IF NOT EXISTS session (
					udid STRING PRIMARY KEY,
					userid INTEGER,
					ipaddr STRING,
					useragent STRING
				);
				"; 
			$db->exec($query); // Create table if not exist
			
			// Clean all input.
			$_POST["username"] = cleanANString($_POST["username"]);
			
			// Check if username exists.
			$query = "SELECT * FROM login WHERE username = '{$_POST['username']}'";
			$result = $db->query($query);
			
			if ($result->fetchArray()) {
				// Username exists!
				printMsg("", "Username '{$_POST['username']}' exists!", "Login", "error");
			}
			else {
				// Username does not exist
				printMsg("", "Your account has been created. You may now login!", "Registration Success", "success");
				$query = sprintf("INSERT INTO login (username, password, name, email, secquestion, secanswer) VALUES (\"%s\", \"%s\", \"%s\", \"%s\", \"%s\", \"%s\")",
					$db->escapeString(strtolower($_POST["username"])),
					$db->escapeString(sha1(AM_HASHSALT . $_POST["password"])),
					$db->escapeString($_POST["name"]),
					$db->escapeString($_POST["email"]),
					$db->escapeString($_POST["secquestion"]),
					$db->escapeString($_POST["secanswer"])
				);
				// htmlentities works for content.
				if ($db->exec($query)) {
					echo "
						<p>
							<b>Hello, {$_POST['name']}!</b>
							<br/>
							Your account has been created. You may now login.
							<br/>
							<br/>
							<a href='?mode=login' class='button scrolly'>Login</a>
						</p>
					";
				}
				else {
					throw new Exception("Could not create account (failed to modify database)");
				}
			}
			
		} catch (Exception $e) {
			echo "
				<span class='errorMessage'><span class='icon fa-warning'></span>
					<b>Unable to create account.</b>
					<br/>
					({$e->getMessage()})
				</span>
				<p>
					<b>Please try again.</b>
					<br/>
					If this message persists, contact an administator using the &quot;Contact Me&quot; form below.
				</p>
			";
		}
	}
	echo "
			</p>
		</article>
	</div>
	";
}
else {
	echo "
	<br/>
	<div class='wrapper style2'>
		<article id='work'>
			<header>
				<h2>Login</h2>
				<p>You need an account to do... stuff.</p>
			</header>
			<p>
				<a href='?mode=login' class='button big scrolly'>Login</a>
				<br />
				<a href='?mode=register' class='button big scrolly'>Sign Up</a>
			</p>
		</article>
	</div>
	";
}
?>
