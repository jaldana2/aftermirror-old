<?php
if (!file_exists("db/status.db") || time() - filemtime("db/status.db") > 60 * 60 * 12) {
	file_put_contents("db/status.db", file_get_contents("https://anime.aftermirror.com/anime/status.db"));
}
$r = readDB("db/status.db");
echo "
<br/>
<div class='wrapper style2'>
	<div class='container' style='min-width: 400px; width: 50%; background-color: white; padding-top: 20px; text-align: center;'>
		<table>
";
//knatsort($r);
foreach ($r as $anime => $data) {
	echo "
		<tr>
			<td style='text-align: right; padding-right: 5px;'>
				<b>{$anime}</b>
			</td>
			<td style='text-align: left; padding-left: 5px;'>
	";
	knatsort($data);
	foreach (array_reverse($data, true) as $episode => $mdata) {
		$mepisode = $mdata["original_title"];
		$episode = ltrim($episode, "0");
		echo "
			<a href='app.Stream?anime={$anime}&episode={$episode}&src={$mepisode}'>Episode {$episode}</a>
			<br/>
		";
	}
	echo "
				<br/>
			</td>
		</tr>
	";
}
echo "
		</table>
	</div>
</div>
";
?>
