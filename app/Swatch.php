<?php
$hostHD = "https://nyc-01.aftermirror.com/anime/high/";
$hostSD = "https://nyc-01.aftermirror.com/anime/low/";

if (isset($_GET["join"])) {
	$uniqid = $_GET["join"];
}
else {
	$uniqid = $_GET["host"];
}
$controls = isset($_GET["controls"]) ? "controls" : "";

echo "
<br/>
<div class='wrapper style2'>
	<article id='work'>
		<div class='container' style='max-width: 800px; text-align: left;'>
			<p>
				<div>
					<span style='float: left;'><b>{$_GET['anime']}</b> Episode {$_GET['episode']}</span>
					<a href='app.Swatch?join={$uniqid}&anime={$_GET['anime']}&episode={$_GET['episode']}&src={$_GET['src']}' onclick=\"alert('right-click and copy the link.'); void(0); return false;\" style='text-decoration: none; float: right;'><span class='fa fa-link'></span> invite URL</a>
					<span style='float: right; margin-right: 5px;' id='ctlStatus'></span>
				</div>
				<video {$controls} id='ctlVideo' style='width: 100%; box-shadow: 0px 0px 4px black;'>
					<source src='{$hostSD}{$_GET['src']}' type='video/mp4' />
				</video>
				<form id='frmChat'>
					<input type='text' id='ctlText' style='font-size: 10px; margin-top: 0px; width: 100%; padding: 1px 4px; color: black;' placeholder='mumble, mumble...' autocomplete='off' />
				</form>
				<div style='position: relative; margin-bottom: -30px; padding-top: 10;'>
					<input class='slidey' type='range' style='width: 100%;' id='ctlTime' min='0' />
					<span id='ctlTimeText' style='position: absolute; right: 0px; top: 20px; font-size: 12px; color: black;'>0:00 / 0:00</span>
				</div>
				<br/>
				<span class='fa fa-play' id='ctlPlayPause'></span> | 
				<span class='fa fa-volume-up' id='ctlVolume'></span> | 
				<span class='fa fa-video-camera' id='ctlQuality'> SD</span>
			</p>
			<div id='ctlMessageBoxCtr'>
				<div id='ctlMessageBox'>
				</div>
			</div>
			<div id='ctlChatOverlay'></div>
		</div>
	</article>
</div>
<script>
	var hdroot = \"{$hostHD}{$_GET['src']}\";
	var sdroot = \"{$hostSD}{$_GET['src']}\";
	var chime = document.createElement('audio');
	var chimeEnabled = true;

	var socket = io.connect('wss://" . $_SERVER['HTTP_HOST'] . ":182/swatch');
	var video = document.getElementById('ctlVideo');
	
	$(function() {
		video = document.getElementById('ctlVideo');
		
		// set chime src
		chime.volume = 0.5;
		chime.src = '/static/chime.mp3';
		socket.on('new user', function(e) {
			if (e.room != '{$uniqid}') return;
			if (!$('#pi_' + e.username).attr('id')) {
				$('#ctlStatus').prepend(
					$('<div>')
						.addClass('ctlPicon')
						.css('background-image', 'url(core.Data?c=profile-picture&v=' + e.username + ')')
						.attr('id', 'pi_' + e.username)
				);
			}
			if (e.role == 'host') {
				parseMsg('system', e.username + ' has created a new room');
			}
			else {
				parseMsg('system', e.username + ' has joined the room');
			}
		});
		socket.on('chat message', function(e) {
			if (e.room != '{$uniqid}') return;
			if (!$('#pi_' + e.username).attr('id')) {
				$('#ctlStatus').prepend(
					$('<div>')
						.addClass('ctlPicon')
						.css('background-image', 'url(core.Data?c=profile-picture&v=' + e.username + ')')
						.attr('id', 'pi_' + e.username)
				);
			}
			parseMsg(e.username, e.message);
		});
		
		$('#ctlText').keypress(function(e) {
			if (e.which == 13) {
				var ctxt = $('#ctlText').val();
				if (ctxt == '/help') {
					parseMsg('system', '<b>Available Commands</b><br/>/help, /chimeoff, /chimeon');
				}
				else if (ctxt == '/chimeoff') {
					chimeEnabled = false;
					parseMsg('system', 'chime disabled');
				}
				else if (ctxt == '/chimeon') {
					chimeEnabled = true;
					parseMsg('system', 'chime enabled');
				}
				else if (ctxt.length > 0) {
					socket.emit('chat message', { message: $('#ctlText').val(), username: '" . LOGIN_USERNAME . "', room: '{$uniqid}' });
				}
				$('#ctlText').val('');
				e.preventDefault();
				return false;
			}
        });
        
        // overloads
       	$('#ctlPlayPause').off('click').on('click', function() {
			if (!video.paused) {
				socket.emit('interact', { action: 'pause', room: '{$uniqid}' });
				socket.emit('chat message', { message: 'paused the episode', username: '" . LOGIN_USERNAME . "', room: '{$uniqid}' });
			}
			else {
				socket.emit('interact', { action: 'play', room: '{$uniqid}' });
				socket.emit('chat message', { message: 'resumed the episode', username: '" . LOGIN_USERNAME . "', room: '{$uniqid}' });
			}
		});
		$('#ctlQuality').off('click').on('click', function() {
			var time = video.currentTime;
			if (hdmode) {
				hdmode = false;
				video.src = sdroot;
				$(this).css('color', 'inherit').text(' SD');
				socket.emit('chat message', { message: 'switched to SD', username: '" . LOGIN_USERNAME . "', room: '{$uniqid}' });
			}
			else {
				hdmode = true;
				video.src = hdroot;
				$(this).css('color', 'black').text(' HD');
				socket.emit('chat message', { message: 'switched to HD', username: '" . LOGIN_USERNAME . "', room: '{$uniqid}' });
			}
			// pause video, and set time to correct time
			video.currentTime = time;
			socket.emit('interact', { action: 'pause', room: '{$uniqid}' });
		});
		$('#ctlTime').off('change').on('change', function() {
			socket.emit('interact', { action: 'seek', value: $(this).val(), room: '{$uniqid}' });
		});
		
		// the actual control
		socket.on('interact', function(e) {
			if (e.room != '{$uniqid}') return;
			if (e.action == 'pause') {
				video.pause();
				$('#ctlPlayPause').removeClass('fa-pause').addClass('fa-play');
			}
			else if (e.action == 'play') {
				video.play();
				$('#ctlPlayPause').removeClass('fa-play').addClass('fa-pause');
			}
			else if (e.action == 'seek') {
				video.pause();
				$('#ctlPlayPause').removeClass('fa-pause').addClass('fa-play');
				video.currentTime = e.value;
			}
		});
		
		// profile UI colors
		socket.on('status', function(e) {
			if (e.room != '{$uniqid}') return;
			if (!$('#pi_' + e.username).attr('id')) {
				$('#ctlStatus').prepend(
					$('<div>')
						.addClass('ctlPicon')
						.css('background-image', 'url(core.Data?c=profile-picture&v=' + e.username + ')')
						.attr('id', 'pi_' + e.username)
				);
			}
			if (e.effect == 'stalled') {
				$('#pi_' + e.username).css('border', 'dotted 4px red');
			}
			else if (e.effect == 'playing') {
				$('#pi_' + e.username).css('border', 'solid 4px limegreen');
			}
			else if (e.effect == 'waiting') {
				$('#pi_' + e.username).css('border', 'dotted 4px goldenrod');
			}
			else if (e.effect == 'paused') {
				$('#pi_' + e.username).css('border', 'dotted 4px goldenrod');
			}
			else if (e.effect == 'ready') {
				$('#pi_' + e.username).css('border', 'dashed 4px limegreen');
			}
		});
		$('#ctlVideo')
			.on('stalled abort error', function(e) {
				socket.emit('interact', { action: 'pause', room: '{$uniqid}' });
				socket.emit('chat message', { message: '<b>stalled, please check status</b>', username: '" . LOGIN_USERNAME . "', room: '{$uniqid}', skipfilter: true });
				socket.emit('status', { effect: 'stalled', username: '" . LOGIN_USERNAME . "', room: '{$uniqid}' });
				updateOverlayLoc();
			})
			.on('play playing', function(e) {
				socket.emit('status', { effect: 'playing', username: '" . LOGIN_USERNAME . "', room: '{$uniqid}' });
				updateOverlayLoc();
			})
			.on('waiting seeking', function(e) {
				socket.emit('status', { effect: 'waiting', username: '" . LOGIN_USERNAME . "', room: '{$uniqid}' });
				updateOverlayLoc();
			})
			.on('pause', function(e) {
				socket.emit('status', { effect: 'paused', username: '" . LOGIN_USERNAME . "', room: '{$uniqid}' });
				updateOverlayLoc();
			})
			.on('canplay canplaythrough', function(e) {
				socket.emit('status', { effect: 'ready', username: '" . LOGIN_USERNAME . "', room: '{$uniqid}' });
				updateOverlayLoc();
			});
		$(window).unload(function() {
			socket.emit('chat message', { message: '<b>" + LOGIN_USERNAME + " left the room.</b>', username: 'system', room: '{$uniqid}', skipfilter: true });
		});
		updateOverlayLoc();
		setInterval('updateOverlayLoc()', 1000);
		setInterval('overlayScrubber()', 1000);
		parseMsg('system', '<b>Available Commands</b><br/>/help, /chimeoff, /chimeon');
	});
	
	function updateOverlayLoc() {
		// chat overlay
		$('#ctlChatOverlay').position({
			my: 'right top',
			at: 'right top',
			of: '#ctlVideo'
		});
	}
	function parseMsg(user, msg) {
		playChime();
		msg = msg.replace(\";Bawl\", \"<img src='/static/audi-icons/Bawl.gif' />\")
			.replace(\";Cam\", \"<img src='/static/audi-icons/Cam.gif' />\")
			.replace(\";Cell\", \"<img src='/static/audi-icons/Cell.gif' />\")
			.replace(\";Com\", \"<img src='/static/audi-icons/Com.gif' />\")
			.replace(\";Cry\", \"<img src='/static/audi-icons/Cry.gif' />\")
			.replace(\";Cute\", \"<img src='/static/audi-icons/Cute.gif' />\")
			.replace(\";Dog\", \"<img src='/static/audi-icons/Dog.gif' />\")
			.replace(\";Etc\", \"<img src='/static/audi-icons/Etc.gif' />\")
			.replace(\";Flu\", \"<img src='/static/audi-icons/Flu.gif' />\")
			.replace(\";Flwr\", \"<img src='/static/audi-icons/Flwr.gif' />\")
			.replace(\";Fury\", \"<img src='/static/audi-icons/Fury.gif' />\")
			.replace(\";Gift\", \"<img src='/static/audi-icons/Gift.gif' />\")
			.replace(\";Grin\", \"<img src='/static/audi-icons/Grin.gif' />\")
			.replace(\";Heat\", \"<img src='/static/audi-icons/Heat.gif' />\")
			.replace(\";Hmph\", \"<img src='/static/audi-icons/Hmph.gif' />\")
			.replace(\";Hset\", \"<img src='/static/audi-icons/Hset.gif' />\")
			.replace(\";Kiss\", \"<img src='/static/audi-icons/Kiss.gif' />\")
			.replace(\";Lol\", \"<img src='/static/audi-icons/Lol.gif' />\")
			.replace(\";Look\", \"<img src='/static/audi-icons/Look.gif' />\")
			.replace(\";Losr\", \"<img src='/static/audi-icons/Losr.gif' />\")
			.replace(\";Love\", \"<img src='/static/audi-icons/Love.gif' />\")
			.replace(\";LuvU\", \"<img src='/static/audi-icons/LuvU.gif' />\")
			.replace(\";Mad\", \"<img src='/static/audi-icons/Mad.gif' />\")
			.replace(\";Meh\", \"<img src='/static/audi-icons/Meh.gif' />\")
			.replace(\";Mkup\", \"<img src='/static/audi-icons/Mkup.gif' />\")
			.replace(\";Nml\", \"<img src='/static/audi-icons/Nml.gif' />\")
			.replace(\";No\", \"<img src='/static/audi-icons/No.gif' />\")
			.replace(\";Ouch\", \"<img src='/static/audi-icons/Ouch.gif' />\")
			.replace(\";Pray\", \"<img src='/static/audi-icons/Pray.gif' />\")
			.replace(\";Sad\", \"<img src='/static/audi-icons/Sad.gif' />\")
			.replace(\";Shy\", \"<img src='/static/audi-icons/Shy.gif' />\")
			.replace(\";Sigh\", \"<img src='/static/audi-icons/Sigh.gif' />\")
			.replace(\";Sly\", \"<img src='/static/audi-icons/Sly.gif' />\")
			.replace(\";Spit\", \"<img src='/static/audi-icons/Spit.gif' />\")
			.replace(\";Spy\", \"<img src='/static/audi-icons/Spy.gif' />\")
			.replace(\";VV\", \"<img src='/static/audi-icons/VV.gif' />\")
			.replace(\";Wave\", \"<img src='/static/audi-icons/Wave.gif' />\")
			.replace(\";What\", \"<img src='/static/audi-icons/What.gif' />\")
			.replace(\";Yawn\", \"<img src='/static/audi-icons/Yawn.gif' />\");
			
		$('#ctlMessageBox').prepend(
			$('<div>')
				.addClass('ctlPost')
				.html('<b>' + user + '</b> <span>' + msg + '</span>')
		);
		$('#ctlChatOverlay').append(
			$('<span>')
				.addClass('ctlChatOverlayPost')
				.attr('timestamp', new Date().getTime() / 1000)
				.html('<span>' + msg + '</span> <b>&laquo; ' + user + '</b><br/>')
				.hide(0).fadeIn(200)
		);
	}
	function overlayScrubber() {
		var time = new Date().getTime() / 1000;
		$('span.ctlChatOverlayPost').each(function() {
			if (time - $(this).attr('timestamp') > 3) {
				$(this).hide(200).remove();
			}
		});
	}
	function playChime() {
		if (!chimeEnabled) return;
		chime.pause();
		chime.currentTime = 0;
		chime.play();
	}
";
if (isset($_GET["join"])) {
	echo "
		socket.emit('user connect', { username: '" . LOGIN_USERNAME . "', room: '{$uniqid}', role: 'client' });
	";
}
else {
	echo "
		socket.emit('user connect', { username: '" . LOGIN_USERNAME . "', room: '{$uniqid}', role: 'host' });
	";
}
echo "
	updateOverlayLoc();
</script>
";
?>
