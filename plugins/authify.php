<?php
if (isset($_COOKIE["am_userid"]) && isset($_COOKIE["am_sessionkey"])) {
	$db = new SQLite3("db/login.db3");
	
	$query = sprintf("SELECT * FROM session WHERE userid = %d AND udid = \"%s\"", $_COOKIE["am_userid"], $_COOKIE["am_sessionkey"]);
	$result = $db->query($query);
	
	if ($result = $result->fetchArray()) {
		define("LOGGED_IN", true);
		define("LOGIN_ID", $_COOKIE["am_userid"]);
		
		$query = sprintf("SELECT * FROM login WHERE userid = %d", $_COOKIE["am_userid"]);
		$result = $db->query($query);
		$result = $result->fetchArray();
		
		define("LOGIN_USERNAME", $result["username"]);
		define("LOGIN_NAME", $result["name"]);
	}
	else {
		define("LOGGED_IN", false);
	}
}
else {
	define("LOGGED_IN", false);
}
?>
