<?php
$_ERRORS = array();

function logError($errorCode, &$_ERRORS) {
	$errorMsg = "";
	
	switch ($errorCode) {
		case "ERROR_NOAPP":
			$errorMsg = "App [" . $_GET["app"] . "] not found";
		break;
		case "ERROR_NOCORE":
			$errorMsg = "App (Core) [" . $_GET["core"] . "] not found";
		break;
		default:
		break;
	}
	
	$callers = debug_backtrace();
	$_ERRORS[] = array("code" => $errorCode, "message" => $errorMsg, "parent" => substr(basename($callers[1]["args"][0]),0,-4));
	
}

function showErrors(&$_ERRORS) {
	if (isSomething($_ERRORS)) {
		echo "<span class='errorMessage'><span class='icon fa-warning'></span>\n";
		foreach ($_ERRORS as $err) {
			echo "Error: {$err['code']} - {$err['message']} ({$err['parent']})<br/>\n";
			echo "<script>console.log(\"[err] - {$err['code']} - {$err['message']} ({$err['parent']})\");</script>\n";
		}
		echo "</span>\n";
		return true;
	}
	return false;
}
function printMsg($code, $message, $parent, $type = "error") {
	switch ($type) {
		case "success":
			echo "
				<span class='successMessage'><span class='icon fa-warning'></span>
				Success: {$code} - {$message} ({$parent})
				</span>
			";
		break;
		default:
			echo "
				<span class='errorMessage'><span class='icon fa-warning'></span>
				Error: {$code} - {$message} ({$parent})
				</span>
			";
		break;
	}
}

?>
